require_relative "scraper/version"
require "capybara/poltergeist"
# require "capybara-webkit"
require "open-uri"
require "dotenv"
Dotenv.load
require "logger"
require "chronic"

module Seek
  class Scraper
    BASE_URL = "http://www.seek.com.au/".freeze
    SEARCH_PATH = "/jobs/#dateRange=:date_range&workType=244&industry=&occupation=:occupation&graduateSearch=false&salaryFrom=0&salaryTo=999999&salaryType=annual&advertiserID=&advertiserGroup=&keywords=:keywords&page=:page&displaySuburb=&seoSuburb=&isAreaUnspecified=false&location=&area=&nation=&sortMode=KeywordRelevance&searchFrom=active+filters+clear+all+classifications&searchType=".freeze
    DEFAULT_DATE_RANGE = 14
    NO_DATE_RANGE = 999
    VALID_DATE_RANGES = [1, 3, 7, 14, 31].freeze
    DEFAULT_LIMIT = 100
    MAX_PAGE = 25
    DATE_FORMAT = "%d %b %Y".freeze
    DEFAULT_SLEEP_TIME = 30 # seconds
    PHANTOMJS_TIMEOUT = 60 # seconds
    USER_AGENTS = ["Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.6.01001)",
                   "Mozilla/5.0 (X11; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0 Iceweasel/36.0",
                   "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0",
                   "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
                   "Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1",
                   "Opera/9.80 (Windows NT 5.1; U; en) Presto/2.10.289 Version/12.01",
                   "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1",
                   "Mozilla/5.0 (Windows NT 6.1; rv:5.0) Gecko/20100101 Firefox/5.02"].freeze


    class Job < Struct.new(:title, :employer, :short_description, :classification,
                           :created_on, :location, :url, :id, :description_html,
                           :salary, :employment_type)
    end

    class Proxy < Struct.new(:host, :port); end

    # Options include:
    #   - date_rage - how far back to search for results (defaults to 14 days, valid values are 1, 3, 7, 14, 31 and nil)
    #   - occupation - a specific occupation to filter search results by, e.g. Web Development & Production
    #                  @see the results of the #list_occupations method for more examples
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the SEEK_PROXIES env var or nil if missing
    #   - sleep_time - how long to pause between consecutive HTTP requests, defaults to 30 seconds
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
      @env = ENV["RACK_ENV"] || ENV["RAILS_ENV"] || "development"
      configure_proxies(options[:proxies])
    end

    def list_occupations
      @ocucupation_list ||= occupations.keys
    end

    # Options include:
    #   - date_rage - how far back to search for results (defaults to 14 days, valid values are 1, 3, 7, 14, 31 and nil)
    #   - occupation - a specific occupation to filter search results by, e.g. Web Development & Production
    #                  @see the results of the #list_occupations method for more examples
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
    #   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888/) -
    #               defaults to reading them from the SEEK_PROXIES env var or nil if missing
    #   - sleep_time - how long to pause between consecutive HTTP requests, defaults to 30 seconds
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    def job_search(keywords, options = {})
      configure_logger(options[:logger])

      @sleep_time = options[:sleep_time] || @options[:sleep_time] || DEFAULT_SLEEP_TIME
      @sleep_time = [@sleep_time, DEFAULT_SLEEP_TIME].min

      limit = (options[:limit] || @options[:limit] || DEFAULT_LIMIT)
      limit = [limit, DEFAULT_LIMIT].min

      max_page = options[:max_page] || @options[:max_page] || MAX_PAGE
      max_page = [max_page, MAX_PAGE].min

      date_range = if options.key?(:date_range)
                     options[:date_range] || NO_DATE_RANGE
                   elsif @options.key?(:date_range)
                     @options[:date_range] || NO_DATE_RANGE
                   end
      date_range = (VALID_DATE_RANGES & [date_range]).first || NO_DATE_RANGE
      jobs_path = SEARCH_PATH.sub(":date_range", date_range.to_s)

      occupation_name = options[:occupation] || @options[:occupation]
      occupation_ids = CGI.escape(occupations[occupation_name] || "")
      jobs_path.sub!(":occupation", occupation_ids)

      kws = CGI.escape(Array(keywords).map(&:downcase).join(" "))
      jobs_path.sub!(":keywords", kws)

      jobs_url = URI.join(BASE_URL, jobs_path).to_s
      jobs = []
      page = 0

      begin
        page += 1
        rest
        next unless visit_through_proxy(jobs_url.sub(":page", page.to_s))

        jobs_page = Nokogiri::HTML(@session.html)
        jobs_page.css("[data-job-id]").each do |job_node|
          rest

          id = job_node["data-job-id"]
          title_link = title = job_node.at_css(".job-title")
          title = title_link.text
          job_path = title_link["href"]
          job_url = URI.join(BASE_URL, job_path).to_s
          employer = job_node.at_css(".advertiser-name").text
          short_description = job_node.at_css(".job-description").text
          classification = job_node.at_css(".classification").text.split(" > ")

          io = open_through_proxy(job_url)
          next unless io

          job_page = Nokogiri::HTML(io)
          created_on = parse_date(job_page.at_css('[itemprop="datePosted"]').text)
          location = job_page.at_css('[itemprop="address"]').text.gsub(/\s{2,}/, ", ").split(", ").reject!(&:empty?)
          description_html = job_page.at_css(".templatetext").inner_html.strip
          salary_node = job_page.at_css('[itemprop="baseSalary"]')
          salary = salary_node.text if salary_node
          employment_type_node = job_page.at_css('[itemprop="employmentType"]')
          employment_type = employment_type_node.text if employment_type_node

          job = Job.new(title, employer, short_description, classification, created_on,
                        location, job_url, id, description_html, salary, employment_type)
          yield(job) if block_given?
          jobs << job

          break if jobs.size == limit
        end
      end while jobs.size < limit && page < max_page

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def rest
      sleep(@sleep_time) unless @env == "test"
    end

    def configure_proxies(proxies = nil)
      @proxies = (proxies || @options[:proxies] || ENV.fetch("SEEK_PROXIES").split(",")).shuffle.map do |proxy|
        uri = URI(proxy)
        Proxy.new(uri.host, uri.port).tap do |proxy|
          Capybara.register_driver :"poltergeist_#{uri.host}" do |app|
            Capybara::Poltergeist::Driver.new(app, timeout: PHANTOMJS_TIMEOUT,
                                              phantomjs_logger: @env == "development" ? STDOUT : Logger.new("/dev/null"),
                                              js_errors: false,
                                              phantomjs_options: ["--proxy=#{proxy.host}:#{proxy.port}", "--load-images=no"])
          end
        end
      end
    end

    def visit_through_proxy(url)
      new_session
      begin
        @session.visit(url)
      rescue => err
        @logger.err "#{err.message} visiting '#{url}'"
        nil
      end
    end

    def open_through_proxy(url)
      proxy = next_proxy
      begin
        open(url, "User-Agent" => USER_AGENTS.sample, "proxy" => "http://#{proxy.host}:#{proxy.port}/")
      rescue => err
        @logger.error "#{err.message} opening '#{url}'"
        nil
      end
    end

    def next_proxy
      @proxies.shift.tap { |proxy| @proxies << proxy }
    end

    def parse_date(date)
      begin
        Date.strptime(date, DATE_FORMAT)
      rescue ArgumentError
        Chronic.parse(date).utc.to_date
      end
    end

    def occupations
      @occupations ||= begin
                         if visit_through_proxy(BASE_URL)
                           doc = Nokogiri::HTML(@session.html)
                           Hash[doc.css("#catoccupation option").map { |option| [option.text, option["value"]] }]
                         else
                           []
                         end
                       end
    end

    def new_session
      @session = Capybara::Session.new(:"poltergeist_#{next_proxy.host}").tap do |session|
        session.driver.headers = { "User-Agent" => USER_AGENTS.sample }
      end
    end
  end
end
