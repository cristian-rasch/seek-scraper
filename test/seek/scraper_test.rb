require File.expand_path("test/test_helper")
require File.expand_path("lib/seek/scraper")

module Seek
  class ScraperTest < Minitest::Test
    def setup
      @seek = Scraper.new
      @keywords = "web developer"
    end

    def test_getting_a_recent_job
      jobs = @seek.job_search(@keywords, limit: 1, date_range: 7)
      assert_equal 1, jobs.size
    end

    def test_getting_three_jobs
      jobs = @seek.job_search(@keywords, limit: 3)
      assert_equal 3, jobs.size
    end

    def test_max_page_option
      jobs = @seek.job_search(@keywords, max_page: 1, limit: 100)
      assert_operator jobs.size, :<=, 50
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @seek.job_search(@keywords, occupation: "Web Development & Production", limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
