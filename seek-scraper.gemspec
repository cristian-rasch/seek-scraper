# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'seek/scraper/version'

Gem::Specification.new do |spec|
  spec.name          = "seek-scraper"
  spec.version       = Seek::Scraper::VERSION
  spec.authors       = ["Cristian Rasch"]
  spec.email         = ["cristianrasch@fastmail.fm"]
  spec.summary       = %q{seek.com.au job scraper}
  spec.homepage      = "https://bitbucket.org/cristian-rasch/seek-scraper"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "poltergeist", "~> 1.6"
  # spec.add_runtime_dependency "capybara-webkit", "~> 1.4"
  spec.add_runtime_dependency "chronic", "~> 0.10"
  spec.add_runtime_dependency "dotenv", "~> 1.0"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.5"
  spec.add_development_dependency "minitest-line", "~> 0.6"
end
