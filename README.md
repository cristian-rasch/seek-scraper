# Seek::Scraper

seek.com.au job scraper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'seek-scraper', git: 'git@bitbucket.org:cristian-rasch/seek-scraper.git', require: 'seek/scraper'
```

And then execute:

    $ bundle

### Installing the PhantomJS headless browser

**IMPORTANT** this library relies on having the PhantomJS binary in your PATH. Make sure to follow the [installation instructions](https://github.com/teampoltergeist/poltergeist#installing-phantomjs).


## Usage

```ruby
require "seek/scraper"
require "pp"

seek = Seek::Scraper.new
# Options include:
#   - date_rage - how far back to search for results (defaults to 14 days, valid values are 1, 3, 7, 14, 31 and nil)
#   - occupation - a specific occupation to filter search results by, e.g. Web Development & Production
#                  @see the results of the #list_occupations method for more examples
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - max_page - the maximum number of pages to scrape (defaults to 25, max. 25)
#   - proxies - a list of HTTP proxies to tunnel WEB traffic through, e.g. %w(http://1.2.3.4:8888/ http://5.6.7.8:8888) -
#               defaults to reading them from the SEEK_PROXIES env var or nil if missing
#   - sleep_time - how long to pause between consecutive HTTP requests, defaults to 30 seconds
#   - logger - a ::Logger instance to log error messages to
pp seek.job_search("web developer", limit: 1)
# #<struct Seek::Scraper::Job
#  title="Web Designer & Front-end web developer",
#  employer="Elcom Technology",
#  short_description=
#   "Fantastic opportunity to join our dynamic team as a Web Designer and Front End Development specialist. Great team and cool location.",
#  classification=
#   ["Information & Communication Technology", "Web Development & Production"],
#  created_on=#<Date: 2015-02-06 ((2457060j,0s,0n),+0s,2299161j)>,
#  location=["Sydney", "CBD", "Inner West & Eastern Suburbs"],
#  url="http://www.seek.com.au/job/28085266?pos=4&type=standout",
#  id="28085266",
#  description_html=
#   "<p><strong>Join Australia's brightest web technology company</strong>​</p>\r\n<p>Elcom is a leading provider of web content management systems to a broad range of businesses across Australia &amp; overseas. We are a flexible and family friendly employer who believes in a work life balance, and that our people are our greatest asset.</p>\r\n<p>We are seeking a web designer to join our dynamic team as a Front-end web developer for a minimum period of 3 months. </p>\r\n<p> <br>\r\n<strong>Your skills and experience will include…</strong></p>\r\n<ul>\r\n<li>Minimum of 2 years experience in web design and web CMS.</li>\n<li>Excellent design skills and attention to detail.</li>\n<li>Highly proficient in HTML, CSS, Photoshop and Dreamweaver.</li>\n<li>Ability to hand-code clean standards-based HTML and CSS.</li>\n<li>Expert level skills in Photoshop, Illustrator.</li>\n<li>Understanding of the latest web standards and technologies.</li>\n<li>Ability to create cross-browser &amp; cross-platform compatible sites</li>\n<li>Knowledge and experience of usability &amp; accessibility.</li>\n<li>Experience with jQuery and JavaScript is an advantage.</li>\n<li>.Net skills would be beneficial </li>\n<li>Ability to work within budget and deadlines</li>\n<li>A team player with the ability to work independently on projects</li>\n<li>Great communication skills</li>\n</ul>\r\n<p>Applicants must be able to demonstrate having worked on a variety of websites and a portfolio of their work online. Please email a resume and a link(s) to your portfolio or websites.</p>",
#  salary=nil,
#  employment_type="Contract/Temp">
```
